@extends('./layout')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-12 col-sm-8 offset-sm-2 col-lg-6 offset-lg-3">
                <b-card class="mt-4">
                    <form-contact></form-contact>
                </b-card>
            </div>
        </div>
    </div>
@endsection
