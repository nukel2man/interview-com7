<div>Detail Contact</div>
<div>Name : {{ $data['name'] }}</div>
<div>E-mail : {{ $data['email'] }}</div>
<div>Phone : {{ $data['phone'] }}</div>
<div>Message : {{ $data['message'] }}</div>
<div>Start Date : {{ $data['start_date'] }}</div>
<div>End Date : {{ $data['end_date'] }}</div>
<div>Need On Site Service : {{ $data['need_on_site_service'] == "1" ? 'Yes' : 'No' }}</div>
<div>Address : {{ $data['address'] }}</div>
