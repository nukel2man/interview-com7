require('./bootstrap');
import Vue from 'vue';
import BootstrapVue from 'bootstrap-vue'
import '../sass/app.scss'
// Import Bootstrap and BootstrapVue CSS files (order is important)
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
// Make BootstrapVue available throughout your project
Vue.use(BootstrapVue)
window.Vue = require('vue');
Vue.component('form-contact', require('./components/FormContact.vue').default);
const app = new Vue({
    el: '#app'
});