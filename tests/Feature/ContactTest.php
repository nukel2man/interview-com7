<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;
use App\Models\Contact;

class ContactTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    use RefreshDatabase;

    public function test_contacts_route_return_ok()
    {
        $response = $this->get('/contact');

        $response->assertStatus(200);
    }

    public function test_contacts_form_validate_fail()
    {
        $params = [
            'name'=>'Wiwat',
            'email'=>'wiwat@gmail', // Email not validate
            'phone'=>'0911926299',
            'message'=>'Hello World!',
            'start_date'=>'2022-10-1 10:10:10',
            'end_date'=>'2022-9-31 10:10:10',// End date not validate
            'need_on_site_service'=> '1',
            'address'=> 'Bangkok, Thailand',
        ];
        $this->post('/api/contact', $params)->assertStatus(400);
    }

    public function test_contacts_validate_store_notification_success()
    {
        $params = [
            'name'=>'Wiwat',
            'email'=>'wiwat@gmail.com',
            'phone'=>'0911926299',
            'message'=>'Hello World!',
            'start_date'=>'2022-10-1 10:10:10',
            'end_date'=>'2022-10-31 10:10:10',
            'need_on_site_service'=> '1',
            'address'=> 'Bangkok, Thailand',
        ];
        $this->post('/api/contact', $params)->assertStatus(201);
    }
    
}
