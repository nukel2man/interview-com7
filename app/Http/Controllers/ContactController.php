<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Mail;
use App\Mail\Notification;
use Illuminate\Http\Request;
use Inertia\Inertia;
use App\Models\Contact;
use Illuminate\Support\Facades\Validator;
use Symfony\Component\HttpFoundation\Response;

class ContactController extends Controller
{

    public function index()
    {
        return view('contact');
    }


    public function store(Request $request)
    {
        $validator = Validator::make(
            $request->all(),
            [
                'name' => ['required', 'max:50'],
                'email' => ['email', 'required', 'max:50'],
                'phone' => ['required', 'max:10'],
                'message' => ['required', 'max:500'],
                'start_date' => ['required', 'date'],
                'end_date' => ['required', 'date', 'after_or_equal:start_date'],
                'address' => ["required_if:need_on_site_service,==,1"]
            ],
            [
                'name.required' => 'Name is required',
                'name.max:50' => 'Name and Email have a max length of 50 characters',
                'email.required' => 'Email is required',
                'email.max:50' => 'Name and Email have a max length of 50 characters',
                'email.email' => 'Email must be a valid email.',
                'phone.required' => 'Phone is required',
                'phone.max:10' => 'Phone is a max length of 50 characters',
                'message.required' => 'Message is a required',
                'message.max:500' => 'Message is a max length of 500 characters',
                'start_date.required' => 'Start Date is a required',
                'end_date.required' => 'End Date is a required',
                'address.required_if' => 'Address is required if `Need on site service` was checked'
            ]
        );
        if ($validator->fails()) {
            return response()->json(['status' => 'validate fail', 'message' => $validator->messages()], Response::HTTP_BAD_REQUEST);
        }
        Contact::create($request->all());

        Mail::to('innovation@comseven.com')->send(new Notification($request->all()));

        return response()->json(['status' => 'success'], Response::HTTP_CREATED);
    }
}
